#!/usr/bin/env python3

import asyncio
import websockets

lobby_timeout = 60*3
servers = []
async def handler(websocket, path):
	recv = (await websocket.recv()).decode("utf-8").split(" ")
	print(f"recv: {recv}")
	if(recv[0] == 'SERVER'):
		ip = websocket.remote_address[0]
		print("server at: " + ip)
		servers.append([ip, websocket])
		try:
			recv = await asyncio.wait_for(websocket.recv(), timeout=lobby_timeout)
		finally:
			servers.remove([ip, websocket])
			print("server closed at: " + ip)
	elif(recv[0] == 'CLIENT'):
		print("client at: " + websocket.remote_address[0])
		await websocket.send(await get_server())
	elif(recv[0] == 'REPORT'):
		print("report: " + recv[1])
		ind = -1
		for i in range(len(servers)):
			if(servers[i][0]):
				ind = i
				break
		if(ind != -1):
			await servers[i][1].send(b"DISCONNECT")
			servers.pop(ind)

async def get_server():
	while True:
		if(len(servers) > 0):
			return servers[0][0]
		await asyncio.sleep(1)

ip = input("IP > ")
start_server = websockets.serve(handler, ip, 5874)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
